Timer HIIT

Il s'agit d'un chronometre spécial qui permet de définir des intervalles intensive et de repos (utile en fitness)

On définit une période d'echauffement
ensuite on définit le nombre de rounds, le temps intense et le temps de repos

le timer se déroule comme ça, pour 2 rounds avec échauffement : échauffement => intense => repos => intense => repos => échauffement

Tout les timers sont en seconde. L'echauffement n'est pas obligatoire.

Entre chaque round, un petit son doit etre jouer pour indiquer le changement d'etat

Pour le dev : projet en Vue JS

- un composant avec le timer et un composant avec le parametrage
- pouvoir commencer, pauser et reset le timer
- pas possible de changer les parametres quand le timer est lancé / en cours
- mobile first, framework CSS possible
